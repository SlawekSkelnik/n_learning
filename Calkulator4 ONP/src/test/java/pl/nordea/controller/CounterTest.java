package pl.nordea.controller;

import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;


/**
 * Created by m010591 on 2017-07-26.
 */
public class CounterTest {

    public static Counter counter;
    public String filtredInputEquation;
    public double actual;
    public double expected;

    @BeforeClass
    public static void setUpBeforeClass() {
        counter = new Counter();
    }


    @Test
    public void shouldCalkulate1ArgInput_TC1() {

        filtredInputEquation = "2";
        expected = 2;
        actual = counter.calculate(filtredInputEquation);
        assertEquals("One argument input does not work for input: " + filtredInputEquation, expected, actual);

        filtredInputEquation = "(2)";
        actual = counter.calculate(filtredInputEquation);
        assertEquals("One argument input does not work when placed in brackets for input: " + filtredInputEquation, expected, actual);

    }

    @Test
    public void shouldCalkulate2ArgsInput_TC2() {

        expected = 4;

        filtredInputEquation = "2+2";
        actual = counter.calculate(filtredInputEquation);
        assertThat("Two argument input does not work for input: " + filtredInputEquation, actual, is(expected));

        filtredInputEquation = "(2+2)";
        actual = counter.calculate(filtredInputEquation);
        assertThat("Two argument input does not work when placed in brackets for input: " + filtredInputEquation, actual, is(expected));

        filtredInputEquation = "(2+(2))";
        actual = counter.calculate(filtredInputEquation);
        assertThat("Two argument input does not work when placed in brackets for input: " + filtredInputEquation, actual, is(expected));

        filtredInputEquation = "((2)+(2))";
        actual = counter.calculate(filtredInputEquation);
        assertThat("Two argument input does not work when placed in brackets for input: " + filtredInputEquation, actual, is(expected));

        filtredInputEquation = "((2)+(2)^1)";
        actual = counter.calculate(filtredInputEquation);
        assertThat("Two argument input does not work when placed in brackets and powerd for input: " + filtredInputEquation, actual, is(expected));

    }

    @Test
    public void shouldCalkulateComplicatedEquation_TC3() {


        expected = 23.5;

        filtredInputEquation = "((2+7)/3+(14-3)*4)/2";
        actual = counter.calculate(filtredInputEquation);
        assertThat("Wrong result for: " + filtredInputEquation, actual, is(expected));

        expected = 3.5;
        //filtredInputEquation = "(3+4*2/(1-5)^((2)^2-2))";
        filtredInputEquation = "(3+4*2/(1-5)^((2)))";
        actual = counter.calculate(filtredInputEquation);
        assertThat("Wrong result for: " + filtredInputEquation, actual, is(expected));


    }




}
