package pl.nordea.controller;

import org.junit.BeforeClass;
import org.junit.Test;
import pl.nordea.model.validation.Filter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.StringContains.containsString;

/**
 * Created by m010591 on 2017-07-26.
 */
public class Filter2BracketsTest {


    public static Filter2Brackets filter2Brackets;
    public String InputEquation;
    public String actual;

    @BeforeClass
    public static void setUpBeforeClass() {
        filter2Brackets = new Filter2Brackets();
    }

    @Test
    public void shouldNotContainEmptyBracket() {

        InputEquation = "jfjslkjf()fds---";
        actual = filter2Brackets.bracketsFilter(InputEquation);
        assertThat("The filtred string: " + actual + " contains empty bracket", actual, not(containsString("()")));

    }

    @Test
    public void shouldNotContainHashSign() {

        InputEquation = "[][{jfjsl56%#{[4kjf$()fds---";
        actual = filter2Brackets.otherSignsFilter(InputEquation);
        assertThat("The filtred string: " + actual + " contains wrong sings like: [$&:;[=?@\\]#|'<>%!{}]", actual, not(containsString("#")));

    }


}
