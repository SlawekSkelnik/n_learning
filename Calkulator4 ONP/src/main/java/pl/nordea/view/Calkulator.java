package pl.nordea.view;

import pl.nordea.controller.Counter;
import pl.nordea.controller.Filter2Brackets;
import pl.nordea.controller.Validator1;
import pl.nordea.model.validation.Filter;
import pl.nordea.model.validation.Validator;

import java.util.Scanner;


/**
 * Created by m010591 on 2017-07-20.
 */
public class Calkulator {


    public static void main(String[] args) {

        while (true) {

            String inputFromUser = getInputFromUser();
            if (inputFromUser.equals("Exit")) break;

            Filter filter2 = new Filter2Brackets();
            String filtredInput = filter2.filterAll(filter2.bracketsFilter(inputFromUser));

            Validator validator1 = new Validator1();
            printResult(filtredInput, validator1);
        }
    }

    private static void printResult(String filtredInput, Validator validator) {
        if (validator.validateAll(filtredInput)) {

            Counter counter = new Counter();
            System.out.println("Oto wynik równania " + filtredInput + " = ");
            System.out.println(counter.calculate(filtredInput));

        } else {
            System.out.println(filtredInput);
        }
    }

    private static String getInputFromUser() {

        System.out.println("Give operations to calkulate (like 5*8 + 6 ) or Exit [e]");
        System.out.print("> ");

        Scanner input = new Scanner(System.in);
        //"3+4*2/(1-5)^2"
        String inputFromUser = input.nextLine();
        if (inputFromUser.equals("e")) {
            System.out.println("Thank you for using our software");
            return "Exit";
        }
        return inputFromUser;
    }
}
