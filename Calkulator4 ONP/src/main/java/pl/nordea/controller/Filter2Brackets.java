package pl.nordea.controller;

import pl.nordea.model.validation.Filter;

/**
 * Created by m010591 on 2017-07-24.
 */
public class Filter2Brackets implements Filter {

    //remove "()" from filter
    @Override
    public String otherSignsFilter(String inputFromUser) {
        return inputFromUser.replaceAll("[$&:;\\[=?@\\]|'<>%#!{}]", "");
    }



    public String bracketsFilter(String inputFromUser) {

        return inputFromUser.
                replaceAll("[(][)]", "");
    }

}
