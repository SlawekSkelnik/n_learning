package pl.nordea.controller;

import pl.nordea.model.operations.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by m010591 on 2017-07-20.
 */
public class Counter {


    public double calculate(String inputFromUser) {

        ArrayList<String> inputArray = getInputArray(inputFromUser);
        List<String> output = getOutput(inputArray);
        return getResult(output);
    }

    private double getResult(List<String> output) {

        LinkedList<Double> stack = new LinkedList<>();


        for (String sign : output) {

            if (sign.matches("[\\d.\\d]{1,}")) {
                stack.push(Double.parseDouble(sign));
            } else if (sign.matches("[+\\-\\*\\/\\^]")) {

                Sign sign1 = getSign(sign.charAt(0));
                double nbr2 = stack.peek();
                stack.pop();
                double nbr1 = stack.peek();
                stack.pop();

                stack.push(count(nbr1, sign1, nbr2));

            }
        }
        return stack.getFirst();

    }

    private List<String> getOutput(List<String> inputArray) {

        List<String> output = new ArrayList<>();
        LinkedList<String> stack = new LinkedList<>();

        int i = 1;
        for (String sign : inputArray) {

            if (sign.matches("[\\d.\\d]{1,}")) {
                output.add(sign);
            } else if (
                    stack.isEmpty() == false &&
                            sign.matches("[+\\-\\*\\/]") &&
                            stack.peek().matches("[\\*\\/]")) {

                output.add(stack.peek());
                stack.pop();
                stack.push(sign);

            } else if (sign.matches("[)]")) {

                boolean notFoundOpenBracket = true;
                while (notFoundOpenBracket) {

                    if (stack.peek().equals("(")) {
                        stack.pop();
                        notFoundOpenBracket = false;
                    } else {
                        output.add(stack.peek());
                        stack.pop();
                    }
                }

            } else if (sign.matches("[+\\-\\*\\/\\^\\(]")) {
                stack.push(sign);
            }
            if (i == inputArray.size()) {

                while (stack.isEmpty() == false) {
                    output.add(stack.peek());
                    if (stack.isEmpty() == false) stack.pop();
                }
            }
            i++;
        }
        return output;
    }

    private ArrayList<String> getInputArray(String inputFromUser) {

        ArrayList<String> inputArray = new ArrayList<String>();
        StringBuilder number = new StringBuilder();
        boolean numberFinded = false;

        char currentSign;
        char nextSign = ' ';

        for (int i = 0; i < inputFromUser.length(); i++) {

            currentSign = inputFromUser.charAt(i);
            if (i + 1 < inputFromUser.length()) {
                nextSign = inputFromUser.charAt(i + 1);
            } else {
                numberFinded = true;
            }

            if (currentSign == Sign.PLUS.getSign() ||
                    currentSign == Sign.MINUS.getSign() ||
                    currentSign == Sign.MULTIPLY.getSign() ||
                    currentSign == Sign.DEVIDE.getSign() ||
                    currentSign == Sign.OPENNING_BRACKET.getSign() || currentSign == Sign.CLOSING_BRACKET.getSign() ||
                    currentSign == Sign.POWER.getSign()) {

                number.append(currentSign);
                numberFinded = true;
            } else {
                number.append(currentSign);
            }

            if (nextSign == Sign.PLUS.getSign() ||
                    nextSign == Sign.MINUS.getSign() ||
                    nextSign == Sign.MULTIPLY.getSign() ||
                    nextSign == Sign.DEVIDE.getSign() ||
                    nextSign == Sign.OPENNING_BRACKET.getSign() || nextSign == Sign.CLOSING_BRACKET.getSign() ||
                    currentSign == Sign.POWER.getSign()) {

                numberFinded = true;
            }
            if (numberFinded) {
                inputArray.add(number.toString());
                number.setLength(0);
                numberFinded = false;
            }
        }

        return inputArray;
    }

    private double count(Double firstNumber, Sign sign, Double secondNumber) {

        Operations operations;

        switch (sign) {

            case DEVIDE:
                operations = new Devide();
                return operations.calculate(firstNumber, secondNumber);
            case MINUS:
                operations = new Minus();
                return operations.calculate(firstNumber, secondNumber);
            case PLUS:
                operations = new Add();
                return operations.calculate(firstNumber, secondNumber);
            case MULTIPLY:
                operations = new Multiply();
                return operations.calculate(firstNumber, secondNumber);
            case POWER:
                operations = new Power();
                return operations.calculate(firstNumber, secondNumber);
        }

        return 0;
    }

    public Sign getSign(char sign) {

        Sign currentSign;

        if (sign == Sign.PLUS.getSign()) {
            currentSign = Sign.PLUS;
        } else if (sign == Sign.MINUS.getSign()) {
            currentSign = Sign.MINUS;
        } else if (sign == Sign.DEVIDE.getSign()) {
            currentSign = Sign.DEVIDE;
        } else if (sign == Sign.MULTIPLY.getSign()) {
            currentSign = Sign.MULTIPLY;
        } else if (sign == Sign.POWER.getSign()) {
            currentSign = Sign.POWER;
        } else {
            currentSign = Sign.WRONG_SIGN;
        }
        return currentSign;

    }
}