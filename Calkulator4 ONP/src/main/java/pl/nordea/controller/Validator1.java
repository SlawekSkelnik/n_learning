package pl.nordea.controller;

import pl.nordea.model.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by m010591 on 2017-07-24.
 */
public class Validator1 implements Validator {

    @Override
    public boolean validateBegining(String equaton) {
        String patern = "^[+\\-\\*\\/].*";
        String foultMessage = "Correct statment, there is operation sign on the beginning: ";

        return validate(equaton, patern, foultMessage);
    }

    @Override
    public boolean validateEnding(String equaton) {

        String patern = ".*[+\\-\\*\\/]$";
        String foultMessage = "Correct statment, there is operation sign on the end: ";

        return validate(equaton, patern, foultMessage);
    }

    @Override
    public boolean validateBeginningAndEnding(String equaton) {

        String patern = "^[+\\-\\*\\/].*[+\\-\\*\\/]$";
        String foultMessage = "Correct statment, there is operation sign on the beginning and ending: ";

        return validate(equaton, patern, foultMessage);
    }

    @Override
    public boolean noDuplicationsInOperatons(String equaton) {

        String pattern = "[+\\-\\*\\/]{2,}";
        Pattern pat = Pattern.compile(pattern);
        Matcher matcher = pat.matcher(equaton);

        if (matcher.find()) {
            System.out.println("Correct statment, there are duplicated signs of oprations :");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean noEmptyInput(String equaton) {

        String patern = "^$";
        String foultMessage = "The input is empty !!!";

        return validate(equaton, patern, foultMessage);
    }


    private boolean validate(String equaton, String patern, String foultMessage) {
        if (equaton.matches(patern)) {
            System.out.println(foultMessage);
            return false;
        } else {
            return true;
        }
    }
}
