package pl.nordea.controller;


import pl.nordea.model.validation.Filter;

/**
 * Created by m010591 on 2017-07-24.
 */
public class Filter1 implements Filter {

    @Override
    public String otherSignsFilter(String inputFromUser) {
        return inputFromUser.replaceAll("[$&:;=?@#|'<>/[/]^()%!{}]", "");
    }

    @Override
    public String bracketsFilter(String inputFromUser) {
        return null;
    }

}
