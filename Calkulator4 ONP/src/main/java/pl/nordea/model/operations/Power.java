package pl.nordea.model.operations;


/**
 * Created by m010591 on 2017-07-25.
 */
public class Power implements Operations {

    @Override
    public double calculate(double number1, double number2) {
        return Math.pow(number1, number2);
    }
}
