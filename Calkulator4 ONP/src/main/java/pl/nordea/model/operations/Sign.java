package pl.nordea.model.operations;

/**
 * Created by m010591 on 2017-07-20.
 */
public enum Sign {

    MINUS('-'), PLUS('+'), DEVIDE('/'), MULTIPLY('*'), POWER('^'),
    OPENNING_BRACKET('('), CLOSING_BRACKET(')'), WRONG_SIGN('?');

    private Character sign;

    Sign(Character sing) {
        this.sign = sing;
    }

    public Character getSign() {
        return sign;
    }
}
