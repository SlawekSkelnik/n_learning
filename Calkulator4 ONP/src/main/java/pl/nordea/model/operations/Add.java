package pl.nordea.model.operations;

/**
 * Created by m010591 on 2017-07-20.
 */
public class Add implements Operations {
    @Override
    public double calculate(double number1, double number2) {
        return number1 + number2;
    }
}
