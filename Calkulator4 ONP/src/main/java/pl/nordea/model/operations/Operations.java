package pl.nordea.model.operations;

/**
 * Created by m010591 on 2017-07-20.
 */
public interface Operations {

    double calculate(double number1, double number2);
}
