package pl.nordea.model.validation;



/**
 * Created by m010591 on 2017-07-24.
 */
public interface Filter {


    default String commaFilter(String inputFromUser) {
        return inputFromUser.replaceAll(",", ".");
    }

    default String spaceFilter(String inputFromUser) {
        return inputFromUser.replaceAll(" ", "");
    }

    default String lettersFilter(String inputFromUser) {
        return inputFromUser.replaceAll("[a-zA-Z]", "");
    }

    String otherSignsFilter(String inputFromUser);

    String bracketsFilter(String inputFromUser);


    default java.lang.String filterAll(String inputFromUser) {

        String filteredInput = commaFilter(inputFromUser);
        filteredInput = spaceFilter(filteredInput);
        filteredInput = lettersFilter(filteredInput);
        filteredInput = otherSignsFilter(filteredInput);

        return filteredInput;

    }

}
