package pl.nordea.model.validation;

/**
 * Created by m010591 on 2017-07-24.
 */
public interface Validator {

    boolean validateBegining(String equaton);

    boolean validateEnding(String equaton);

    boolean validateBeginningAndEnding(String equaton);

    boolean noDuplicationsInOperatons(String equaton);

    boolean noEmptyInput(String equaton);

    default boolean validateAll(String equation) {

        if (validateBeginningAndEnding(equation)&&
                validateBegining(equation) &&
                validateEnding(equation) &&
                noDuplicationsInOperatons(equation) &&
                noEmptyInput(equation)) {

            return true;
        } else {
            return false;
        }
    }

}
